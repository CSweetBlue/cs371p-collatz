// --------------------------------
// projects/c++/collatz/Collatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// --------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <math.h>   // log2
#include <unordered_map> // unordered_map

#include "Collatz.h"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int storageArray[500000] = {0};

int calculateCycleLength(long val) {
    int counter = 1;
    long originalVal = val;

    while (val != 1) {
        if (val < 500000 && storageArray[val] != 0) {
            counter += storageArray[val];
            val = 1;
        }
        // Checks if val is power of two, and if so, just use log2 to solve.
        else if ((val & (val-1)) == 0) {
            counter += log2(val);
            break;
        }
        else if (val % 2 == 0) {
            ++counter;
            val = val / 2;
        }
        else {
            ++++counter;
            val = (3*val + 1) / 2;
        }
    }

    if (originalVal < 500000) {
        storageArray[originalVal] = counter - 1;
    }

    return counter;
}

int collatz_eval (int i, int j) {

    assert(i > 0 && j > 0);
    //assert(i < 1000000 && j < 1000000);

    int maxCount = 0;

    int val = (i > j ? j : i);
    int end = (i > j ? i : j);

    for (; val <= end; ++val) {
        int returned = calculateCycleLength(val);
        if (maxCount < returned) {
            maxCount = returned;
        }
    }

    assert(maxCount > 0);

    return maxCount;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
